<%--
  Created by IntelliJ IDEA.
  User: Neo89
  Time: 16:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="panel panel-primary">

  <div class="panel-heading" role="tab" id="headingDiv_${param.sectionNo}">
    <h4 class="panel-title">
      <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#${param.sectionNo}"
         aria-expanded="false" aria-controls="${param.sectionNo}">
        ${param.headingName}
      </a>
    </h4>
  </div>

  <div id="${param.sectionNo}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingLabel_${param.sectionNo}">
    <div class="panel-body">
      ${param.pageContent}
    </div>
  </div>

</div>
