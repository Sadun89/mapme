<%--
  Created by IntelliJ IDEA.
  User: Neo89
  Time: 16:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="locationFormModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header ">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center"> Save Location For Me</h4>
      </div>

      <div class="modal-body">

        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab_a" data-toggle="tab">General </a></li>
          <li><a href="#tab_b" data-toggle="tab">Location </a></li>
          <li><a href="#tab_c" data-toggle="tab">Map Location</a></li>
          <li><a href="#tab_d" data-toggle="tab">Other </a></li>
        </ul>

        <div class="tab-content">

          <div class="tab-pane active" id="tab_a">
            <div class="row"></div>
            <div class="container-fluid">
              <div class="form-group ">
                <label class="col-sm-2 control-label" for="textinput">Category</label>
                <div class="col-sm-10">
                  <input id="textinput" name="textinput" placeholder="Add.." class="input-xlarge form-control" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="locationName">Name</label>
                <div class="col-sm-10">
                  <input id="locationName" name="locationName" placeholder="location name" class="input-xlarge form-control" required="" type="text">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="locationDescription">Description</label>
                <div class="col-sm-10">
                  <textarea id="locationDescription" class="form-control" name="locationDescription">add location description</textarea>
                </div>
              </div>
            </div>
          </div>




          <div class="tab-pane" id="tab_b">
              <div class="row"></div>
              <div class="container-fluid">
                <%--https://developers.google.com/maps/documentation/javascript/examples/places-autocomplete-addressform--%>
                <div class="form-group ">
                  <label class="col-sm-2 control-label" for="street_number"> Find Location </label>
                  <div id="locationField" class="col-sm-10">
                    <input id="autocomplete" name="textinput"  class="input-xlarge field form-control" type="text" onFocus="geolocate()" >
                  </div>
                </div>


                <div class="form-group ">
                  <label class="col-sm-2 control-label" for="street_number">Street Number</label>
                  <div class="col-sm-10">
                    <input id="street_number" name="textinput"  class="input-xlarge field form-control" type="text">
                  </div>
                </div>

                <div class="form-group ">
                  <label class="col-sm-2 control-label" for="route">Route</label>
                  <div class="col-sm-10">
                    <input id="route" name="route"  class="input-xlarge field form-control" type="text">
                  </div>
                </div>


                <div class="form-group ">
                  <label class="col-sm-2 control-label" for="locality"> Locality </label>
                  <div class="col-sm-10">
                    <input id="locality" name="locality"  class="input-xlarge form-control" type="text">
                  </div>
                </div>


                <div class="form-group ">
                  <label class="col-sm-2 control-label" for="administrative_area_level_1"> administrative_area_level_1 </label>
                  <div class="col-sm-10">
                    <input id="administrative_area_level_1" name="textinput"  class="input-xlarge field form-control" type="text">
                  </div>
                </div>


                <div class="form-group ">
                  <label class="col-sm-2 control-label" for="postal_code">Postal Code</label>
                  <div class="col-sm-10">
                    <input id="postal_code" name="textinput"  class="input-xlarge field form-control" type="text">
                  </div>
                </div>

                <div class="form-group ">
                  <label class="col-sm-2 control-label" for="country"> Country </label>
                  <div class="col-sm-10">
                    <input id="country" name="textinput"  class="input-xlarge field form-control" type="text">
                  </div>
                </div>

              </div>
          </div>




          <div class="tab-pane" id="tab_c">
            <div class="row"></div>
            <div class="container-fluid">
              <div class="form-group">
                <label class="col-sm-2 control-label" > Set Location </label>
                <div class="col-sm-10">

                </div>
              </div>
            </div>
          </div>



          <div class="tab-pane" id="tab_d">
            <h4>Other Section</h4>
            <p>Other Location Details Other Location Details Other Location Details</p>
          </div>

        </div>
      </div>


      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>

    </div>
  </div>
</div>
