<%--
  Created by IntelliJ IDEA.
  User: Neo89
  Date: 4/5/2015
  Time: 18:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"/>


  <style>


    html, body {
      background-color:#e9eaed;
    }
    .content {
      width:960px;
      height:0px;
      margin-right: auto;
      margin-left: auto;
    }
    .panel-group {
      width:430px;
      z-index: 100;
      -webkit-backface-visibility: hidden;
      -webkit-transform: translateX(-100%) rotate(-90deg);
      -webkit-transform-origin: right top;
      -moz-transform: translateX(-100%) rotate(-90deg);
      -moz-transform-origin: right top;
      -o-transform: translateX(-100%) rotate(-90deg);
      -o-transform-origin: right top;
      transform: translateX(-100%) rotate(-90deg);
      transform-origin: right top;
    }
    .panel-heading {
      width: 430px;
    }
    .panel-title {
      height:18px
    }
    .panel-title a {
      float:right;
      text-decoration:none;
      padding: 10px 430px;
      margin: -10px -430px;
    }
    .panel-body {
      height:auto;
      width:500px;
    }
    .panelCollapse {
      height:auto;
      width:500px;
    }
    .myRotateBody{
      -webkit-backface-visibility: hidden;
      -webkit-transform: translateX(-100%) rotate(90deg);
      -webkit-transform-origin: right top;
      -moz-transform: translateX(-100%) rotate(90deg);
      -moz-transform-origin: right top;
      -o-transform: translateX(-100%) rotate(90deg);
      -o-transform-origin: right top;

      transform: translateX(-100%) rotate(90deg);
      transform-origin: right top;

    /*  transform: translateX(0%) rotate(90deg);
      transform-origin: left 0 0;*/
    }

    .bodySection {
      width: auto;
      -ms-transform: rotate(90deg);  /*IE 9*/
      -webkit-transform: rotate(90deg);  /*Safari*/
      transform: rotate(90deg);
    }



    .panel-group .panel img {
      margin-left:400px;
      margin-top:800px;
      position: absolute;
    }





/*
    .panel-horizontal {
      display:table;
      width:100%;
    }
    .panel-horizontal > .panel-heading, .panel.panel-horizontal > .panel-body, .panel.panel-horizontal > .panel-footer {
      display:table-cell;
    }
    .panel-horizontal > .panel-heading, .panel.panel-horizontal > .panel-footer {
      width: 25%;
      border:0;
      vertical-align: middle;
    }
    .panel-horizontal > .panel-heading {
      border-right: 1px solid #ddd;
      border-top-right-radius: 0;
      border-bottom-left-radius: 4px;
    }
    .panel-horizontal > .panel-footer {
      border-left: 1px solid #ddd;
      border-top-left-radius: 0;
      border-bottom-right-radius: 4px;
    }
*/


  </style>

</head>
<body>
<div class="container">
  <div class="row">
    <div class="content">

      <div class="panel-group horizontalPanel" id="accordion">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> General </a>
            </h4>

          </div>
          <div id="collapseOne" class="panel-collapse panelCollapse collapse in">
            <div class="panel-body bodySection">
              <div class="row">
                <div class="col-lg-6">
                  <!-- Text input-->
                  <div class="form-group ">
                    <label class="col-sm-2 control-label" for="textinput">Category</label>
                    <div class="col-sm-10">
                      <input id="textinput" name="textinput" placeholder="Add.." class="input-xlarge form-control" type="text">
                    </div>
                  </div>

                  <!-- Text input-->
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="locationName">Name</label>
                    <div class="col-sm-10">
                      <input id="locationName" name="locationName" placeholder="location name" class="input-xlarge form-control" required="" type="text">
                    </div>
                  </div>

                </div>
                <div class="col-lg-6">
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="locationDescription">Description</label>
                  <div class="col-sm-10">
                    <textarea id="locationDescription" class="form-control" name="locationDescription">add location description</textarea>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>




        <%-- Map Section--%>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"> Map Section </a>
            </h4>

          </div>
          <div id="collapseTwo" class="panel-collapse  panelCollapse collapse">
            <div class="panel-body">
              Map Section
              <!-- Textarea -->
              <div class="form-group">
                <label class="col-sm-2 control-label" > Set Location </label>
                <div class="col-sm-10">


                </div>
              </div>
            </div>
          </div>
        </div>



        <%-- Other Section--%>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"> Other </a>
            </h4>

          </div>
          <div id="collapseThree" class="panel-collapse panelCollapse collapse">
            <div class="panel-body">
              Other Section
            </div>
          </div>
        </div>
      </div>


    <%--<div class="container">
      <div class="row">
        <div class="content">
          <div class="panel-group" id="accordion">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    Suzuki
                  </a>
                </h4>

              </div>
              <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                  <div class="myRotateBody">
                    dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    Chevrolet
                  </a>
                </h4>

              </div>
              <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body ">
                  <div class="myRotateBody">
                    aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaff
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading" >
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                    Volvo
                  </a>
                </h4>

              </div>
              <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                  <div class="myRotateBody">
                    olololololololoolsdofokkdsjgijbhusdfhdsgjhbsdgsd
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>--%>




      <%--<div class="panel-group horizontalPanel" id="accordion">

      <div class="panel panel-default panel-horizontal">
        <div class="panel-heading">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> <h3 class="panel-title">Panel title</h3> </a>
        </div>

        <div id="collapseOne" class="panel-collapse collapse in">
          <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque saepe iusto nulla molestias quis esse incidunt veritatis exercitationem dolore officiis. Nam quos iusto officiis ullam itaque voluptatum fuga assumenda culpa!</div>
        </div>
          <div class="panel-footer">Panel footer</div>
      </div>



      <div class="panel panel-default panel-horizontal">
        <div class="panel-heading">

          <h3 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> <h3 class="panel-title">Panel 2</h3> </a></h3>
        </div>

        <div id="collapseThree" class="panel-collapse collapse">
          <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque saepe iusto nulla molestias quis esse incidunt veritatis exercitationem dolore officiis. Nam quos iusto officiis ullam itaque voluptatum fuga assumenda culpa!</div>
        </div>
        <div class="panel-footer">Panel footer</div>
      </div>


      <div class="panel panel-default panel-horizontal">
        <div class="panel-heading">
          <h3 class="panel-title">Panel title</h3>
        </div>
        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque saepe iusto nulla molestias quis esse incidunt veritatis exercitationem dolore officiis. Nam quos iusto officiis ullam itaque voluptatum fuga assumenda culpa!</div>
        <div class="panel-footer">Panel footer</div>
      </div>

</div>--%>


    </div>
  </div>
</div>
</body>
</html>
