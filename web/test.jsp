<%--
  Created by IntelliJ IDEA.
  User: Neo89
  Date: 3/25/2015
  Time: 23:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <title>Place details</title>
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
  <meta charset="utf-8">
  <style>
    html, body, #map-canvas {
      height: 50%;
      margin: 10%;
      padding: 5px
    }
  </style>
  <%--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>--%>
  <script>

    function initialize() {
      var map = new google.maps.Map("#map-canvas", {
            center: new google.maps.LatLng(-33.8665433, 151.1956316),
            zoom: 15
      });

      var request = {
        placeId: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
      };

      var infowindow = new google.maps.InfoWindow();

      var service = new google.maps.places.PlacesService(map);

      service.getDetails(request, function(place, status) {
        if (status == google.maps.places.PlacesServiceStatus.OK) {
          var marker = new google.maps.Marker({
            map: map,
            position: place.geometry.location
          });
          google.maps.event.addListener(marker, 'click', function() {
            infowindow.setContent(place.name);
            infowindow.open(map, this);
          });
        }
      });

    }


    google.maps.event.addDomListener(window, 'load', initialize);

  </script>
</head>
<body>
<div id="map-canvas"></div>
</body>
</html>