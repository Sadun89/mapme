<%--
  Created by IntelliJ IDEA.
  User: Neo89
  Time: 20:08
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="container-fluid">
  <nav class="navbar navbar-default" role="navigation">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="col-md-2">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span
                class="icon-bar"></span><span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#"> Code Lanka  </a>
      </div>
    </div>

    <div class="col-md-10">

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

        <ul class="nav navbar-nav">
          <li> <a href="#"> <span class="glyphicon glyphicon-home"> </span>  GDG Mapper  </a> </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="glyphicon glyphicon-list-alt"></span>  Categories  <b class="caret"></b></a>
            <ul class="dropdown-menu">

              <li><a href="#"> Buddhist Temples </a></li>
              <li><a href="#"> Famous  </a></li>
              <li><a href="#"> Something else...</a></li>

              <li class="divider"></li>
              <li><a href="#">IT Company</a></li>

              <li class="divider"></li>
              <li><a href="#">One more separated link</a></li>

            </ul>
          </li>
        </ul>

        <ul class="nav navbar-nav">
          <li> <a href="#" data-backdrop="static" data-toggle="modal" data-target="#locationFormModal">
                <span class="glyphicon glyphicon-plus"> </span>  Add Mapper </a>
          </li>

        <%--  <button type="button" class="btn btn-primary" data-backdrop="static" data-toggle="modal" data-target="#locationFormModal">
            Large modal
          </button>--%>

        </ul>
        <ul class="nav navbar-nav">
            <li>
                  <%--<h1> Project Map Me</h1>--%>
            </li>
          </ul>



        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="glyphicon glyphicon-comment"></span>  Chats <span class="label label-primary">42</span>
            </a>
            <ul class="dropdown-menu">
              <li><a href="#"><span class="label label-warning">7:00 AM</span> Hi :) </a></li>
              <li><a href="#"><span class="label label-warning">8:00 AM</span> How are you? </a></li>
              <li><a href="#"><span class="label label-warning">9:00 AM</span> What are you doing? </a></li>
              <li class="divider"></li>
              <li><a href="#" class="text-center">View All</a></li>
            </ul>
          </li>

          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="glyphicon glyphicon-envelope"></span> Inbox <span class="label label-info">32</span>
            </a>
            <ul class="dropdown-menu">
              <li><a href="#"><span class="label label-warning">4:00 AM</span>Favourites Snippet</a></li>
              <li><a href="#"><span class="label label-warning">4:30 AM</span>Email marketing</a></li>
              <li><a href="#"><span class="label label-warning">5:00 AM</span>Subscriber focused email
                design</a></li>
              <li class="divider"></li>
              <li><a href="#" class="text-center">View All</a></li>
            </ul>
          </li>

          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="glyphicon glyphicon-user"></span> System Setting  <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="#"><span class="glyphicon glyphicon-user"> </span> Profile</a></li>
              <li><a href="#"><span class="glyphicon glyphicon-cog"> </span> Settings</a></li>
              <li class="divider"></li>
              <li><a href="/logout"> <span class="glyphicon glyphicon-off"></span> Log Out</a> </li>
            </ul>
          </li>
        </ul>

      </div>


    </div>

  </nav>
</div>

