<%--
  Created by IntelliJ IDEA.
  User: Neo89
  Date: 3/25/2015
  Time: 19:56
  To change this template use File | Settings | File Templates.
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <jsp:include page="/layout/head.jsp" />
       <%-- <script>
            var map;
            function initialize() {
                var mapOptions = {
                    zoom: 8,
                    center: new google.maps.LatLng(-34.397, 150.644)
                };
                map = new google.maps.Map(document.getElementById('map-canvas'),
                        mapOptions);
            }

            google.maps.event.addDomListener(window, 'load', initialize);

        </script>--%>
    </head>



    <body>
        <jsp:include page="/layout/menu.jsp" />

        <jsp:include page="/view/locationPage.jsp"/>

        <!-- begin template navbar-->
        <div id="map-canvas"></div>
        <!-- end template -->


        <div class="container-fluid" id="main">

        <div class="row">
            <div class="col-xs-7" id="left">

                <h2> Let Me Show Where It is </h2>

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    <c:forEach begin="0" end="10" varStatus="i">
                        <jsp:include page="/view/locationDetailsPage.jsp">
                            <jsp:param name="sectionNo" value="${i.index}"/>
                            <jsp:param name="headingName" value="Heading_${i.index}"/>
                            <jsp:param name="pageContent" value="body_${i.index}body_${i.index}body_${i.index}
                            body_${i.index}body_${i.index}body_${i.index}body_${i.index}
                            body_${i.index}body_${i.index}body_${i.index}body_${i.index}
                            body_${i.index}body_${i.index}body_${i.index}body_${i.index}
                            body_${i.index}body_${i.index}body_${i.index}body_${i.index}
                            body_${i.index}body_${i.index}body_${i.index}body_${i.index}
                            body_${i.index}body_${i.index}body_${i.index}body_${i.index}
                            body_${i.index}body_${i.index}body_${i.index}body_${i.index}
                            body_${i.index}body_${i.index}body_${i.index}body_${i.index}
                            body_${i.index}body_${i.index}body_${i.index}body_${i.index}
                            body_${i.index}body_${i.index}body_${i.index}body_${i.index}
                            body_${i.index}body_${i.index}body_${i.index}body_${i.index}
                            body_${i.index}body_${i.index}body_${i.index}body_${i.index}
                            body_${i.index}body_${i.index}body_${i.index}body_${i.index}
                            body_${i.index}body_${i.index}body_${i.index}body_${i.index}
                            body_${i.index}body_${i.index}body_${i.index}body_${i.index}
                            body_${i.index}"/>
                        </jsp:include>
                    </c:forEach>

                    <hr>
                </div>

            </div>


            <div class="col-xs-5"><!--map-canvas will be postioned here--></div>

        </div>

</div>



    </body>
</html>
